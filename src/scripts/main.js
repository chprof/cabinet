document.addEventListener("DOMContentLoaded", () => {
  svg4everybody();
  $('[data-element="toggle"]').on('click', function(e) {
    if($(this).attr('data-body-class')) {
      $('body').toggleClass('scroll-hidden')
    }
    $(`#${$(this).attr('data-target')}`).toggleClass('active')
  })
  $('[data-element="calendar"]').datepicker({
    maxViewMode: 2,
    format: 'dd.mm.yyyy hh:ii',
    language: "ru",
    todayHighlight: true,
    toggleActive: true,
    datesDisabled: ['15.04.2021', '08.04.2021', '16.04.2021'],
  });
});